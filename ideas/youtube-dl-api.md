---
category: Web
title: youtube-dl API
short: Serves youtube-dl as a Rest API
author: fossdd
---

A Webserver serving the youtube-dl in various variants.

Be able to return:
- RSS (of a channel given or a playlist)
- JSON (simple response from `youtube-dl -j`)

At best do that in python and use the youtube-dl as package. See https://github.com/ytdl-org/youtube-dl#embedding-youtube-dl