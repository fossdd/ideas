---
category: Android
title: OSM Height Writer
short: Reads's device height and set it to the nodes around 10 meters
author: fossdd
---

App that always get's device height, and maybe if you open the app, it shows height's collected for nodes (maybe only named). Then you can check the nodes with it's new height, write the changelog message, and push to the server via a API.