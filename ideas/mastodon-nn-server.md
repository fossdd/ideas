---
category: Network Neutrality
title: Mastodon pro-netneutrality server
short: prevents the spread of links to exclusive walled-gardens
author: libBletchley
---

Similar to the [client](mastodon-nn-client.md) project.  Posts with
Tor-hostile and/or CloudFlare links are refused or edited to respect
network neutrality.
