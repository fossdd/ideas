---
category: Android
title: Mind Training App
short: You have to keep to the exact order, if you press wrong, the game starts all over again.
author: mondstern
---

Here you see the "playing field":
A blue, a green, a yellow and a red button.

![4 buttons](https://codeberg.org/mondstern/artworks/raw/branch/master/mindgame.jpg)

the game is, you have to keep the exact order, if you press wrong, the game starts all over again.

### example:
the blue button flashes briefly, then you have to press the blue button.
the blue button flashes briefly and then the green button flashes briefly.
then you have to press the blue button and then the green button.
the blue button flashes briefly, then the green button flashes briefly and then the yellow button flashes.
you must then press the blue button first, then the green button and then the yellow button.

to make the sequence continue in a different way, it continues in a different way...

the blue button flashes, then the green, then the yellow and then the green.
so you always have to keep to the same sequence.

### Variations:

I can think of the following game variations - in the app settings you could also set the following how you could play:

* 4 coloured squares
* 4 coloured circles
* the 4 numbers 1, 2, 3 and 4
* 4 coloured squares and write the wrong colours on them so that you have to concentrate more, e.g. write green on blue, write red on green, write yellow on red and write blue on yellow.
* assign a tone to each colour and then play with the 4 colours and the tone.
* no colour flashes anymore and only the tones sound and you have to press on the colour that had that tone. 
* Difficulty levels: easy, medium and difficult, so that you have less and less time for the new colours and/or sounds.

and at the end with a highscore list so that you know how far you have got.