---
category: Security
title: Cookie publisher
short: An app or background service or firefox-plugin, which spans a p2p network and then distribute (tracker) cookies.
author: kollo
---

The idea is, that the personal profiles generated from these coockies becomes weaker and at the end worthless, when many profiles are mixed together.
Normally, when a coockie appears on a new machine, the algorithms can assume, that the new machine also belongs to the same user. Ans so, all coockies found on that new machine are linked to the profile. What each individual coockie means and wherther it is encrypted or just a hash doesnt matter.

So far, we are missing an automatic way to share coockies. Of course one need to have blacklists of coockies not to be shared, e.g. the functional ones which are used to identify a user login etc.
