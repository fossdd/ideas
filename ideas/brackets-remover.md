---
category: Math
title: Brackets remover
short: Small tool that get's a string and return a string without math brackets, that are not essential.
author: fossdd
---

Remove math brackets that can removed.

For example: `(1*1)+2`

Here the brackets can removed. 

More information is here: https://en.wikipedia.org/wiki/Order_of_operations#Mnemonics

Or for german reader, a maybe better understandable version: https://de.wikipedia.org/wiki/Punktrechnung_vor_Strichrechnung