---
category: Math
title: Console Calculator
short: A CLI tool that is structured like a shell where you can type math commands.
author: fossdd
---

A CLI tool that is structured like a shell where you can type math commands. These results will be printed and if wanted added in a variable. Also be able to calculate the args. Maybe implement a own file format where you can run commands. `--json` would nice to have.  Also convert latex code like `\cdot` to their value.

## Additional notes
- 