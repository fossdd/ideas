---
category: Network Neutrality
title: search client
short: expand Surfraw to harvest search results into a local db for filtering
author: libBletchley
---

# Problem:

To the well-informed privacy seeker, search engines give a lot of junk
results.  Normies are mostly oblivious to all the Tor hostile links
and CloudFlare sites that all but one search engine returns.
CloudFlare thrives on this ignorance.  And the ignorance keeps the
sites that disrespect network neutrality on top, which keeps them
popular, in a vicious cycle that's ruining the web and shrinking the
neutral network.

# Solution

CLI search tool with results imported to a local db for filtering and
analysis before presenting to the user.  E.g. CloudFlare results could
be given a reduced ranking.  "Surfraw", a commandline tool by Julian
Assange, could perhaps be modified to accommodate post-processing.  Or
Surfraw could be inspiration to create a whole new tool.

Extra features:

* feed the data from each search into a YaCy instance -- but only
  after filtering out CloudFlare sites.
