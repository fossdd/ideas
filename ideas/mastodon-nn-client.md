---
category: Network Neutrality
title: Mastodon pro-netneutrality client
short: help prevent users from spreading links to exclusive walled-gardens and handle bad inbound links.
author: libBletchley
---

# Problem:

Mastodon users are unwittingly posting links that lead to
netneutrality-abusing sites like CloudFlare.  They don't know the
problems those exclusive links cause.

Tor users get a toot with a CF link, they click on it, and get a CAPTCHA.  It's a hassle and waste of time.  

# Solution

* When posting: checks your link to see if the document has access
  restrictions (e.g. paywall, blocks tor, CloudFlare).  If yes, it
  edits your toot with an archive.org version of the link and keeps
  you in the editor.

* When reading: posts containing bad links are hidden or replacement
  links are attached.
  
Version 2 features:

* Stats are kept so chronic offenders can be unfollowed, muted, or
  targeted for etiquette pursuasion/gentle lecturing.
