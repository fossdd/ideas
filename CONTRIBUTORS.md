Thanks to our great Contributors, that let these ideas grow.

<!-- contributors.start -->
### fossdd
- [**OSM Height Writer**](ideas/openstreetmap-height-reader.md) - [Reads's device height and set it to the nodes around 10 meters](ideas/openstreetmap-height-reader.md)
- [**OpenStreetMap Messaging App**](ideas/openstreetmap-messaging.md) - [Messenger over the OSM API like every another messenger.](ideas/openstreetmap-messaging.md)
- [**Wikipedia Android-App Fork**](ideas/wikipedia-android-fork.md) - [Fork the offical wikipedia app and make it able to use for every mediawiki server.](ideas/wikipedia-android-fork.md)
- [**youtube-dl API**](ideas/youtube-dl-api.md) - [Serves youtube-dl as a Rest API](ideas/youtube-dl-api.md)
- [**Console Calculator**](ideas/console-calculator.md) - [A CLI tool that is structured like a shell where you can type math commands.](ideas/console-calculator.md)
- [**Brackets remover**](ideas/brackets-remover.md) - [Small tool that get's a string and return a string without math brackets, that are not essential.](ideas/brackets-remover.md)
- [**Fastlane structures Generator**](ideas/fastlane-structures-generator.md) - [Generate fastlane structures as a CLI](ideas/fastlane-structures-generator.md)
- [**HTML/CSS minifier**](ideas/html-minifier.md) - [A minifier that is kept to the lowest needs](ideas/html-minifier.md)
- [**StackExchange App**](ideas/stackexchange-app.md) - [Simple client for StackExchange sites like StackOverFlow](ideas/stackexchange-app.md)
### mondstern
- [**ADB Webcam**](ideas/adb-webcam.md) - [You can use your smartphone camera as a webcam via ADB](ideas/adb-webcam.md)
- [**Watermark App**](ideas/watermark-app.md) - [Simple app that watermarks images](ideas/watermark-app.md)
- [**Hours Calculator App**](ideas/hours-calculator-app.md) - [Calculate your day and say how it was](ideas/hours-calculator-app.md)
- [**Railway Station Photos Countries Game**](ideas/railway-station-photos-countries-game.md) - [A brain-train game around the Railway Station Photos project](ideas/railway-station-photos-countries-game.md)
- [**Mow the Lawn Game**](ideas/mow-the-lawn-game.md) - [A android FOSS game where you mow the lawn](ideas/mow-the-lawn-game.md)
- [**Mind Training App**](ideas/mind-training-app.md) - [You have to keep to the exact order, if you press wrong, the game starts all over again.](ideas/mind-training-app.md)
- [**F-Droid Game**](ideas/f-droid-game.md) - [A F-Droid apps icons guessing game](ideas/f-droid-game.md)
### kollo
- [**Cookie publisher**](ideas/cookie-publisher.md) - [An app or background service or firefox-plugin, which spans a p2p network and then distribute (tracker) cookies.](ideas/cookie-publisher.md)
<!-- contributors.end -->