# App idea

Fork this repo, and you can easily add a file to the [`ideas`](ideas) repository, with the [`TEMPLATE.md`](TEMPLATE.md) as template.

Or the easier way is to open a simple issue.

# Code Suggestions 

Do your code and open a Pull Request. `pylint` and `black` python checks would great!